<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Finance */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Finances'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= Yii::t('app', 'Finance').' '. Html::encode($this->title) ?></h2>
        </div>
        <?php
            if(Yii::$app->user->identity->id == $model->project->manager0->id){
                echo Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']).'  ';
                echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]);
            }
        ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'description',
        'type',
        'amount',
        'payment_time',
        [
            'attribute' => 'project.name',
            'label' => Yii::t('app', 'Project'),
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>Project</h4>
    </div>
    <?php 
    $gridColumnProject = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'start_date',
        'deadline',
        'end_date',
        [
            'attribute' => 'manager0.username',
            'label' => 'Manager',
        ],
        [
            'attribute' => 'client0.name',
            'label' => 'Client',
        ],
    ];
    echo DetailView::widget([
        'model' => $model->project,
        'attributes' => $gridColumnProject    ]);
    ?>
</div>
