<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Worker */

?>
<div class="worker-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->user->name . ' ' . $model->user->surname) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        [
            'attribute' => 'user.username',
            'label' => Yii::t('app', 'User'),
        ],
        [
            'attribute' => 'projectPart.job',
            'label' => Yii::t('app', 'Project Part'),
        ],
        'role',
        'time_spent:integer',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>