<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Worker */

$this->title = $model->user->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Workers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="worker-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?=Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a(Yii::t('app', 'Save As New'), ['save-as-new', 'user_id' => $model->user_id, 'project_part_id' => $model->project_part_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'user_id' => $model->user_id, 'project_part_id' => $model->project_part_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'user_id' => $model->user_id, 'project_part_id' => $model->project_part_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        [
            'attribute' => 'user.username',
            'label' => Yii::t('app', 'User'),
        ],
        [
            'attribute' => 'projectPart.job',
            'label' => Yii::t('app', 'Project Part'),
        ],
        'role',
        'time_spent:integer',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>Project Part</h4>
    </div>
    <?php 
    $gridColumnProjectPart = [
        ['attribute' => 'id', 'visible' => false],
        'job',
        'man_hour',
        'percentage_done',
        'start_date',
        'end_date',
		['attribute' => 'project.name',
		'label' => Yii::t('app', 'Project')],
    ];
    echo DetailView::widget([
        'model' => $model->projectPart,
        'attributes' => $gridColumnProjectPart    ]);
    ?>
    <div class="row">
        <h4>User</h4>
    </div>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
		'name',
		'surname',
        'username',
        'email',
    ];
    echo DetailView::widget([
        'model' => $model->user,
        'attributes' => $gridColumnUser    ]);
    ?>
</div>
