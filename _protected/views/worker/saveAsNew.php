<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Worker */

$this->title = Yii::t('app', 'Save As New {modelClass}: ', [
    'modelClass' => 'Worker',
]). ' ' . $model->user_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Workers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->user_id, 'url' => ['view', 'user_id' => $model->user_id, 'project_part_id' => $model->project_part_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Save As New');
?>
<div class="worker-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
