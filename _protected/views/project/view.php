<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Project */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php 
	function test() {
		echo "<script>alert(1);</script";
	}
?>
<div class="project-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
		<?php
		
            if(Yii::$app->user->identity->id == $model->manager0->id){
                echo Html::a(Yii::t('app', 'Save As New'), ['save-as-new', 'id' => $model->id], ['class' => 'btn btn-info']).'  ';
                echo Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']).'  ';
                echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
				]) . '  ';
            }
			 echo Html::a(Yii::t('app', 'Get Report'), ['get-report', 'id' => $model->id], ['class' => 'btn btn-default']).'  ';
        ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'start_date',
        'deadline',
        'end_date',
        [
            'attribute' => 'manager0.username',
            'label' => Yii::t('app', 'Manager'),
        ],
        [
            'attribute' => 'client0.name',
            'label' => Yii::t('app', 'Client'),
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerFinance->totalCount){
    $gridColumnFinance = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'description',
            'type',
            'amount',
            'payment_time',
                ];
    echo Gridview::widget([
        'dataProvider' => $providerFinance,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-finance']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Finance')),
        ],
        'export' => false,
        'columns' => $gridColumnFinance
    ]);
}
?>

    </div>
    <div class="row">
        <h4>Client</h4>
    </div>
    <?php 
    $gridColumnParticipant = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'email',
        'phone_number',
        'type',
    ];
    echo DetailView::widget([
        'model' => $model->client0,
        'attributes' => $gridColumnParticipant    ]);
    ?>
    <div class="row">
        <h4>Manager</h4>
    </div>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
		'name',
		'surname',
        'username',
        'email',
    ];
    echo DetailView::widget([
        'model' => $model->manager0,
        'attributes' => $gridColumnUser    ]);
    ?>
    
    <div class="row">
<?php
if($providerProjectPart->totalCount){
    $gridColumnProjectPart = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'job',
            'man_hour',
            'percentage_done',
            'start_date',
            'end_date',
                ];
    echo Gridview::widget([
        'dataProvider' => $providerProjectPart,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-project-part']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Project Part')),
        ],
        'export' => false,
        'columns' => $gridColumnProjectPart
    ]);
}
?>

    </div>
</div>
