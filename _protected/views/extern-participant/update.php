<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ExternParticipant */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Extern Participant',
]) . ' ' . $model->participant_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Extern Participants'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->participant_id, 'url' => ['view', 'participant_id' => $model->participant_id, 'project_part_id' => $model->project_part_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="extern-participant-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
