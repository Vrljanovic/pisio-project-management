<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\ExternParticipant */

$this->title = $model->participant_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Extern Participants'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="extern-participant-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= Yii::t('app', 'Extern Participant').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a(Yii::t('app', 'Save As New'), ['save-as-new', 'participant_id' => $model->participant_id, 'project_part_id' => $model->project_part_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'participant_id' => $model->participant_id, 'project_part_id' => $model->project_part_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'participant_id' => $model->participant_id, 'project_part_id' => $model->project_part_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        [
            'attribute' => 'participant.name',
            'label' => Yii::t('app', 'Participant'),
        ],
        [
            'attribute' => 'projectPart.job',
            'label' => Yii::t('app', 'Project Part'),
        ],
        'role',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>Participant<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnParticipant = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'email',
        'phone_number',
        'type',
    ];
    echo DetailView::widget([
        'model' => $model->participant,
        'attributes' => $gridColumnParticipant    ]);
    ?>
    <div class="row">
        <h4>ProjectPart<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnProjectPart = [
        ['attribute' => 'id', 'visible' => false],
        'job',
        'man_hour',
        'percentage_done',
        'start_date',
        'end_date',
        'project_id',
    ];
    echo DetailView::widget([
        'model' => $model->projectPart,
        'attributes' => $gridColumnProjectPart    ]);
    ?>
</div>
