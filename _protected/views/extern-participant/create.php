<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ExternParticipant */

$this->title = Yii::t('app', 'Create Extern Participant');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Extern Participants'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="extern-participant-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
