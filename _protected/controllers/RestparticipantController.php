<?php
namespace app\controllers;

use yii\rest\ActiveController;
use Yii;
use app\models\Participant;
use app\models\ExternParticipant;

class RestparticipantController extends ActiveController
{
   public $modelClass = 'app\models\Participant';
   
   
   public function actionGetall(){
		
		$participants = Participant::find()->all();
		$externParticipants = ExternParticipant::find()->all();
		
		foreach ($participants as &$pp) {
			foreach($externParticipants as $epp){
				if ($pp->id == $epp->participant->id){
					$result[] = [
						'participant_info' => $epp->participant,
						'extern_info' => [
							'project_part' => $epp->projectPart,
							'role' => $epp->role
						]
					];
				} 
			}
			
			$hasElement = false;
			if(isset($result)){
				foreach ($result as &$el) {
					if($el['participant_info']['id'] == $pp->id){
						$hasElement = true;
						break;
					}
				}
			}
			if(!$hasElement)
				$result[] = [ 'participant_info' => $pp ];
		}
		
		return $result;
	}
}