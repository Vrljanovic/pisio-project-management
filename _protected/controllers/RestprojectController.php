<?php
namespace app\controllers;

use yii\rest\ActiveController;
use app\models\Project;
use app\models\User;
use yii;

class RestprojectController extends ActiveController
{
   public $modelClass = 'app\models\Project';
   
    public function actions() {
		$actions = parent::actions();
		unset($actions['index']);
		unset($actions['create']);
		return $actions;
	}

   public function actionIndex() {
		$array = Project::find()->all();
		foreach($array as &$value){
			$obj[] = [
				'id' => $value->id,
				'name' => $value->name,
				'start_date' => $value->start_date,
				'deadline' => $value->deadline,
				'end_date' => $value->end_date,
				'manager_name' => $value->manager0->name . ' ' . $value->manager0->surname,
				'manager_email' => $value->manager0->email,
				'manager_username' => $value->manager0->username,
				'client_name' => $value->client0->name,
				'client_email' => $value->client0->email,
				'client_phone' => $value->client0->phone_number,
				'project_parts' => $value->projectParts,
				'finances' => $value->finances,
			];
		}
		return $obj;
   }
   
   public function actionCreate() {
		if(!Yii::$app->request->isPost) {
		   Yii::$app->response->statusCode = 405;
		   return "Not allowed";
	   }
		$manager = User::find()->where(['=', 'username', Yii::$app->request->post('manager')])->asArray()->all()[0];
		$project = new Project();
		$project->name = Yii::$app->request->post('name');
		$project->start_date = Yii::$app->request->post('startDate');
		$project->deadline = Yii::$app->request->post('deadline');
		$project->end_date = Yii::$app->request->post('endDate');
		$project->manager = $manager['id'];
		$project->client = Yii::$app->request->post('client');
		
		if($project->saveAll()) {
			Yii::$app->response->statusCode = 200;
			return 'Success';
		}
		Yii::$app->response->statusCode = 400;
		return 'Error';
		
   }
   
   public function actionGetone($id) {
	   return 'aaa';
   }
}