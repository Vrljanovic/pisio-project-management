<?php
namespace app\controllers;

use yii\rest\ActiveController;
use yii;
use app\models\User;

class RestuserController extends ActiveController
{
   public $modelClass = 'app\models\User';

	public function actions() {
		$actions = parent::actions();
		unset($actions['index']);
		return $actions;
	}

   public function actionIndex() {
	$array = User::find()->orderBy('id')->asArray()->all();
	foreach($array as &$value)
		unset($value['password_hash'],$value['updated_at'],$value['created_at'],$value['account_activation_token'],$value['password_reset_token'],$value['auth_key'],$value['status']);
    return $array;
   }
	
	public function actionLogin(){
	   if(!Yii::$app->request->isPost) {
		   Yii::$app->response->statusCode = 405;
		   return "Not allowed";
	   }
	   $username = Yii::$app->request->post('username');
	   $password = Yii::$app->request->post('password');
	   $user = User::find()->where(['=', 'username', $username])->asArray()->all()[0];
	   if(password_verify($password, $user['password_hash'])){
		   Yii::$app->response->statusCode = 200;
	   }
	   else {
		   Yii::$app->response->statusCode = 403;
	   }
   }
}