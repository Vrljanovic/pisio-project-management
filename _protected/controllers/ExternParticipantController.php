<?php

namespace app\controllers;

use Yii;
use app\models\ExternParticipant;
use app\models\ExternParticipantSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ExternParticipantController implements the CRUD actions for ExternParticipant model.
 */
class ExternParticipantController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ExternParticipant models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ExternParticipantSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ExternParticipant model.
     * @param integer $participant_id
     * @param integer $project_part_id
     * @return mixed
     */
    public function actionView($participant_id, $project_part_id)
    {
        $model = $this->findModel($participant_id, $project_part_id);
        return $this->render('view', [
            'model' => $this->findModel($participant_id, $project_part_id),
        ]);
    }

    /**
     * Creates a new ExternParticipant model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ExternParticipant();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'participant_id' => $model->participant_id, 'project_part_id' => $model->project_part_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ExternParticipant model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $participant_id
     * @param integer $project_part_id
     * @return mixed
     */
    public function actionUpdate($participant_id, $project_part_id)
    {
        if (Yii::$app->request->post('_asnew') == '1') {
            $model = new ExternParticipant();
        }else{
            $model = $this->findModel($participant_id, $project_part_id);
        }

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'participant_id' => $model->participant_id, 'project_part_id' => $model->project_part_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ExternParticipant model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $participant_id
     * @param integer $project_part_id
     * @return mixed
     */
    public function actionDelete($participant_id, $project_part_id)
    {
        $this->findModel($participant_id, $project_part_id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    /**
    * Creates a new ExternParticipant model by another data,
    * so user don't need to input all field from scratch.
    * If creation is successful, the browser will be redirected to the 'view' page.
    *
    * @param mixed $id
    * @return mixed
    */
    public function actionSaveAsNew($participant_id, $project_part_id) {
        $model = new ExternParticipant();

        if (Yii::$app->request->post('_asnew') != '1') {
            $model = $this->findModel($participant_id, $project_part_id);
        }
    
        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'participant_id' => $model->participant_id, 'project_part_id' => $model->project_part_id]);
        } else {
            return $this->render('saveAsNew', [
                'model' => $model,
            ]);
        }
    }
    
    /**
     * Finds the ExternParticipant model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $participant_id
     * @param integer $project_part_id
     * @return ExternParticipant the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($participant_id, $project_part_id)
    {
        if (($model = ExternParticipant::findOne(['participant_id' => $participant_id, 'project_part_id' => $project_part_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}
