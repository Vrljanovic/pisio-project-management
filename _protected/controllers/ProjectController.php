<?php

namespace app\controllers;

use Yii;
use app\models\Project;
use app\models\ProjectSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProjectController implements the CRUD actions for Project model.
 */
class ProjectController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $providerFinance = new \yii\data\ArrayDataProvider([
            'allModels' => $model->finances,
        ]);
        $providerProjectPart = new \yii\data\ArrayDataProvider([
            'allModels' => $model->projectParts,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerFinance' => $providerFinance,
            'providerProjectPart' => $providerProjectPart,
        ]);
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Project();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->request->post('_asnew') == '1') {
            $model = new Project();
        }else{
            $model = $this->findModel($id);
        }

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Project model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    /**
    * Creates a new Project model by another data,
    * so user don't need to input all field from scratch.
    * If creation is successful, the browser will be redirected to the 'view' page.
    *
    * @param mixed $id
    * @return mixed
    */
    public function actionSaveAsNew($id) {
        $model = new Project();

        if (Yii::$app->request->post('_asnew') != '1') {
            $model = $this->findModel($id);
        }
    
        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('saveAsNew', [
                'model' => $model,
            ]);
        }
    }
    
    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for Finance
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddFinance()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Finance');
            if (!empty($row)) {
                $row = array_values($row);
            }
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formFinance', ['row' => $row]);
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for ProjectPart
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddProjectPart()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('ProjectPart');
            if (!empty($row)) {
                $row = array_values($row);
            }
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formProjectPart', ['row' => $row]);
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
	
	public function actionGetReport($id){
		$project = $this->findModel($id);
		
		require_once __DIR__ . '/../vendor/autoload.php';
		$mpdf = new \Mpdf\Mpdf();
		
		$mpdf->setTitle('Project Export -' . $project->name);
		$mpdf->writeHTML('<h1 align="center">' . $project->name.'</h1>');
		$mpdf->writeHTML('Start date: ' . $project->start_date);
		$mpdf->writeHTML('Project deadline: ' . $project->deadline);
		$mpdf->writeHTML('Project finished: ' . ($project->end_date == null ? 'Not finished' : $project->end_date));
		$mpdf->writeHTML('Manager: ' . $project->manager0->name . ' ' . $project->manager0->surname);
		$mpdf->writeHTML('Client: ' . $project->client0->name);
		$mpdf->writeHTML('<br>');
		$mpdf->writeHTML('<h2>Finance:</h2>');
		$finNum=1;
		$mpdf->writeHTML('<p style="text-indent: 30px">(Number. Description, Type, Amount, Payment Time)</p>'); 
		foreach($project->finances as $finance){
			$mpdf->writeHTML('<p style="text-indent: 30px">' . $finNum . '. ' . $finance->description . ', ' . $finance->type . 
			', ' . $finance->amount . ', ' . $finance->payment_time . '</p>');
			$finNum++;
		}
		$mpdf->writeHTML('<h2>Project Parts:</h3>');
		$brProj=1;
		foreach($project->projectParts as $part){
			$mpdf->writeHTML('<h4 style="text-indent: 30px">' . $brProj.'. Project Part - '.$part->job.'</h4>');
			$mpdf->writeHTML('<p style="text-indent: 60px">Man hour: ' . $part->man_hour . '</p>');
			$mpdf->writeHTML('<p style="text-indent: 60px">Percentage done: ' . $part->percentage_done . '%</p>');
			$mpdf->writeHTML('<p style="text-indent: 60px">Start date: ' . $part->start_date . '</p>');
			$mpdf->writeHTML('<p style="text-indent: 60px">End date: ' . $part->end_date . '</p>');
			$mpdf->writeHTML('<h5 style="text-indent: 60px">Workers on '.$part->job.':</h5>');
			foreach($part->workers as $worker){
				$mpdf->writeHTML('<p style="text-indent: 90px">Name: ' . $worker->user->name . ' ' . $worker->user->surname . '</p>');
				$mpdf->writeHTML('<p style="text-indent: 90px">Role:' . $worker->role . '</p>');
				$mpdf->writeHTML('<p style="text-indent: 90px">Hours spent: ' . $worker->time_spent . '</p><br>');
			}
			$mpdf->writeHTML('<h5 style="text-indent: 60px">Extern participants on '.$part->job.':</h5>');
			foreach($part->externParticipants as $extern){
				$mpdf->writeHTML('<p style="text-indent: 60px">Name: ' . $extern->participant->name . '</p>');
				$mpdf->writeHTML('<p style="text-indent: 60px">Email: ' . $extern->participant->email . '</p>');
				$mpdf->writeHTML('<p style="text-indent: 60px">Phone number: ' . $extern->participant->phone_number . '</p>');
				$mpdf->writeHTML('<p style="text-indent: 60px">Role:' . $extern->role . '</p>');
			}
		}
		
		//D - Force Download*/
		$mpdf->Output('Project Report -' . $project->name . '.pdf', 'D');
		
		 return $this->render('view', [
            'model' => $this->findModel($id),
            'providerFinance' => $providerFinance,
            'providerProjectPart' => $providerProjectPart,
        ]);
	}
		
}
