<?php
namespace app\controllers;

use yii\rest\ActiveController;
use app\models\Finance;

class RestfinanceController extends ActiveController
{
   public $modelClass = 'app\models\Finance';
   
      public function actions() {
		$actions = parent::actions();
		unset($actions['index']);
		return $actions;
	}

   public function actionIndex() {
		$array = Finance::find()->all();
		foreach($array as &$value){
			$obj[] = [
				'payment_info' => $value,
				'project_info' => $value->project
			];
		}
		return $obj;
   }
}