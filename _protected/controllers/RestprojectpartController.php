<?php
namespace app\controllers;

use yii\rest\ActiveController;

class RestprojectpartController extends ActiveController
{
   public $modelClass = 'app\models\ProjectPart';
}