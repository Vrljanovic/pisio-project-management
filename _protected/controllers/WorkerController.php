<?php

namespace app\controllers;

use Yii;
use app\models\Worker;
use app\models\WorkerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * WorkerController implements the CRUD actions for Worker model.
 */
class WorkerController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Worker models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new WorkerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Worker model.
     * @param integer $user_id
     * @param integer $project_part_id
     * @return mixed
     */
    public function actionView($user_id, $project_part_id)
    {
        $model = $this->findModel($user_id, $project_part_id);
        return $this->render('view', [
            'model' => $this->findModel($user_id, $project_part_id),
        ]);
    }

    /**
     * Creates a new Worker model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Worker();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'user_id' => $model->user_id, 'project_part_id' => $model->project_part_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Worker model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $user_id
     * @param integer $project_part_id
     * @return mixed
     */
    public function actionUpdate($user_id, $project_part_id)
    {
        if (Yii::$app->request->post('_asnew') == '1') {
            $model = new Worker();
        }else{
            $model = $this->findModel($user_id, $project_part_id);
        }

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'user_id' => $model->user_id, 'project_part_id' => $model->project_part_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Worker model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $user_id
     * @param integer $project_part_id
     * @return mixed
     */
    public function actionDelete($user_id, $project_part_id)
    {
        $this->findModel($user_id, $project_part_id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    /**
    * Creates a new Worker model by another data,
    * so user don't need to input all field from scratch.
    * If creation is successful, the browser will be redirected to the 'view' page.
    *
    * @param mixed $id
    * @return mixed
    */
    public function actionSaveAsNew($user_id, $project_part_id) {
        $model = new Worker();

        if (Yii::$app->request->post('_asnew') != '1') {
            $model = $this->findModel($user_id, $project_part_id);
        }
    
        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'user_id' => $model->user_id, 'project_part_id' => $model->project_part_id]);
        } else {
            return $this->render('saveAsNew', [
                'model' => $model,
            ]);
        }
    }
    
    /**
     * Finds the Worker model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $user_id
     * @param integer $project_part_id
     * @return Worker the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($user_id, $project_part_id)
    {
        if (($model = Worker::findOne(['user_id' => $user_id, 'project_part_id' => $project_part_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}
