<?php
namespace app\controllers;

use yii\rest\ActiveController;
use app\models\ExternParticipant;

class RestexternparticipantController extends ActiveController
{
   public $modelClass = 'app\models\ExternParticipant';
   
       public function actions() {
		$actions = parent::actions();
		unset($actions['index']);
		return $actions;
	}

   public function actionIndex() {
		$array = ExternParticipant::find()->all();
		foreach($array as &$value){
			$obj[] = [
				'name' => $value->participant->name,
				'email' => $value->participant->email,
				'phone_number' => $value->participant->phone_number,
				'role' => $value->role,
				'project_part' => $value->projectPart
			];
		}
		return $obj;
   }
}