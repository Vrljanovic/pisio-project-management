<?php
namespace app\controllers;

use yii\rest\ActiveController;
use app\models\Worker;
use app\models\User;
use app\models\Project;
use yii;

class RestworkerController extends ActiveController
{
   public $modelClass = 'app\models\Worker';
   
   public function actions() {
		$actions = parent::actions();
		unset($actions['index']);
		unset($actions['create']);
		return $actions;
	}

   public function actionIndex() {
	$array = Worker::find()->all();
	foreach($array as &$value){
		$project = Project::findOne($value->projectPart->project_id);
		$obj[] = [
			'name' => $value->user->name . ' ' .$value->user->surname,
			'email' => $value->user->email,
			'username' => $value->user->username,
			'projectPart' => $value->projectPart->job,
			'project' => $project->name
		];
	}
	return $obj;
   }
   
   public function actionCreate(){
		if(!Yii::$app->request->isPost) {
		   Yii::$app->response->statusCode = 405;
		   return "Not allowed";
	   }
		$worker = new Worker();
		$worker->user_id = Yii::$app->request->post('user_id');
		$worker->project_part_id = Yii::$app->request->post('project_part_id');
		$worker->role = Yii::$app->request->post('role');
		$worker->time_spent = Yii::$app->request->post('time_spent');
		
		try{
			if($worker->saveAll()) {
				Yii::$app->response->statusCode = 200;
				return 'Success';
			}
			Yii::$app->response->statusCode = 400;
			return 'Error';
		}catch (\yii\db\IntegrityException $ex) {
			if ($ex->errorInfo[0] == 23000){
				Yii::$app->response->statusCode = 418;
				return 'Existing Worker';
			}
			else
				Yii::$app->response->statusCode = 400;
				return 'Unknown Error';
		}
		
   }
}