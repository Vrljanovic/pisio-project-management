<?php

namespace app\models;

use Yii;
use \app\models\base\Finance as BaseFinance;

/**
 * This is the model class for table "finance".
 */
class Finance extends BaseFinance
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['description', 'type', 'amount', 'payment_time', 'project_id'], 'required'],
            [['amount'], 'number'],
            [['payment_time'], 'safe'],
            [['project_id'], 'integer'],
            [['description'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 45]
        ]);
    }
	
}
