<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "project".
 *
 * @property integer $id
 * @property string $name
 * @property string $start_date
 * @property string $deadline
 * @property string $end_date
 * @property integer $manager
 * @property integer $client
 *
 * @property \app\models\Finance[] $finances
 * @property \app\models\Participant $client0
 * @property \app\models\User $manager0
 * @property \app\models\ProjectPart[] $projectParts
 */
class Project extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'finances',
            'client0',
            'manager0',
            'projectParts'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'start_date', 'deadline', 'manager'], 'required'],
            [['start_date', 'deadline', 'end_date'], 'safe'],
            [['manager', 'client'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'start_date' => Yii::t('app', 'Start Date'),
            'deadline' => Yii::t('app', 'Deadline'),
            'end_date' => Yii::t('app', 'End Date'),
            'manager' => Yii::t('app', 'Manager'),
            'client' => Yii::t('app', 'Client'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinances()
    {
        return $this->hasMany(\app\models\Finance::className(), ['project_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient0()
    {
        return $this->hasOne(\app\models\Participant::className(), ['id' => 'client']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager0()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'manager']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectParts()
    {
        return $this->hasMany(\app\models\ProjectPart::className(), ['project_id' => 'id']);
    }
    

    /**
     * @inheritdoc
     * @return \app\models\ProjectQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ProjectQuery(get_called_class());
    }
}
