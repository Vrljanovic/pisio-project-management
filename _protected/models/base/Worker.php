<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "worker".
 *
 * @property integer $user_id
 * @property integer $project_part_id
 * @property string $role
 * @property integer $time_spent
 *
 * @property \app\models\ProjectPart $projectPart
 * @property \app\models\User $user
 */
class Worker extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'projectPart',
            'user'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'project_part_id', 'time_spent'], 'required'],
            [['user_id', 'project_part_id', 'time_spent'], 'integer'],
            [['role'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'worker';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'project_part_id' => Yii::t('app', 'Project Part ID'),
            'role' => Yii::t('app', 'Role'),
            'time_spent' => Yii::t('app', 'Time Spent'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectPart()
    {
        return $this->hasOne(\app\models\ProjectPart::className(), ['id' => 'project_part_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'user_id']);
    }
    

    /**
     * @inheritdoc
     * @return \app\models\WorkerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\WorkerQuery(get_called_class());
    }
}
