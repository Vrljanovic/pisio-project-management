<?php

namespace app\models;

use Yii;
use \app\models\base\ProjectPart as BaseProjectPart;

/**
 * This is the model class for table "project_part".
 */
class ProjectPart extends BaseProjectPart
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['job', 'man_hour', 'start_date', 'end_date', 'project_id'], 'required'],
            [['man_hour', 'project_id'], 'integer'],
            [['start_date', 'end_date'], 'safe'],
            [['job'], 'string', 'max' => 255],
            [['percentage_done'], 'string', 'max' => 4]
        ]);
    }
	
}
