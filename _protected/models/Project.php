<?php

namespace app\models;

use Yii;
use \app\models\base\Project as BaseProject;

/**
 * This is the model class for table "project".
 */
class Project extends BaseProject
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name', 'start_date', 'deadline', 'manager'], 'required'],
            [['start_date', 'deadline', 'end_date'], 'safe'],
            [['manager', 'client'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ]);
    }
	
}
