<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ProjectPart]].
 *
 * @see ProjectPart
 */
class ProjectPartQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ProjectPart[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ProjectPart|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
