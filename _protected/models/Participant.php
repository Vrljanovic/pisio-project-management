<?php

namespace app\models;

use Yii;
use \app\models\base\Participant as BaseParticipant;

/**
 * This is the model class for table "participant".
 */
class Participant extends BaseParticipant
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name', 'email', 'phone_number'], 'required'],
            [['name', 'email', 'phone_number'], 'string', 'max' => 45],
            [['type'], 'string', 'max' => 255]
        ]);
    }
	
}
